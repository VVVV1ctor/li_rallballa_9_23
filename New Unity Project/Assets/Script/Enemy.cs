﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class Enemy : MonoBehaviour
{
    GameObject player;// renference to player object 
    Rigidbody rb;// Reference to myself
    public float speed;// How fast enemy can move, set in editor
                       // Start is called before the first frame update

    public Text gameoverText;


    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        rb = GetComponent<Rigidbody>();//find myself rigidbody
                                       //gameoverText = ""; In this point, I do same things as wintext, but it not reference unity engine UI.

    }

    // Update is called once per frame
    void Update()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        Vector3 dirToPlayer = player.transform.position - transform.position;// The direction to player
        dirToPlayer = dirToPlayer.normalized; //convert from different length to length of 1

        rb.AddForce(dirToPlayer * speed);//move in direction

    }
    void OnTriggerEnter(Collider other)//
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.SetActive(false);//destory player by touch it
            //SetgameoverText();



        }

    }
}
        //void SetgameoverText()
        //{

           
                //gameoverText = "gameover!";

                //Invoke("RestarLevel", 2f);// Call restart level after 5 second, but it's not work 




           
        //}
    




