﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;// Using the scene managing mode in this script


public class playerconrtal : MonoBehaviour
{
    public float speed;// int the value speed, and make it display on editor 
    public Text countText;
    public Text winText;
    
    private Rigidbody rb;//reference player rigidbody
    private int count;// int the value"count"
    


    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;// count is zero when the game begining

        SetCountText();
        winText.text = "";
       

    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            speed = 10;
        }
        if (Input.GetKey(KeyCode.LeftShift))
        {
                speed = 5;
        }
        
    }//player use space bar to boost speed; player use Leftshift to set the speed back to normal. This make player have controll for their speed to dodge enemy

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");//input player's position for moving
        float moveVertical = Input.GetAxis("Vertical");//

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
        
    }
    void OnTriggerEnter(Collider other) 
    {
        if (other.gameObject.CompareTag("pick up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();// if player touch pickup, count+1, pickup disappear


        }
        if (other.gameObject.CompareTag("paperwall"))
        {
            other.gameObject.SetActive(false);// if player touch paper wall, the wall destory. player could use this mechanism to get in some room.
        }
       
           

    }
    void SetCountText()
    {
        countText.text = "Count:" + count.ToString();
        if (count > 12)// If player count> 12 text and level active
        {
            winText.text = "You Win!";

            Invoke("RestarLevel", 2f);// Call restart level after 5 second, but it's not work 

        }
    }
    void Restartlevel()
    {

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        //Reload the current Level
    }

}

